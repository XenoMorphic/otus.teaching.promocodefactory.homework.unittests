﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    class PartnerBuilder
    {
        bool _isActive;
        List<PartnerPromoCodeLimit> _promocodeLimits;
        int _numberIssuedPromoCodes;

        public PartnerBuilder WithCreatedIsActive(bool isActive)
        {
            this._isActive = isActive;
            return this;
        }

        public PartnerBuilder WithCreatedNumberIssuePromocodes(int numberOfPromocodes)
        {
            this._numberIssuedPromoCodes = numberOfPromocodes;
            return this;
        }

        public PartnerBuilder AddLimit(PartnerPromoCodeLimit limit)
        {
            this._promocodeLimits.Add(limit);
            return this;
        }

        public Partner Build()
        {
            return new Partner()
            {
                IsActive = _isActive,
                PartnerLimits = _promocodeLimits,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes
            };
        }

        public PartnerBuilder()
        {
            _promocodeLimits = new List<PartnerPromoCodeLimit>();
        }
    }
}
