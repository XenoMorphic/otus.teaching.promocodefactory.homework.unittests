﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().WithCreatedIsActive(false).Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        private SetPartnerPromoCodeLimitRequest CreateBaseSetPartnerPromoCodeLimitRequest(int limit)
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today,
                Limit = limit
            };
        }

        private PartnerPromoCodeLimit CreateBasePartnerPromoCodeLimit(int limit, DateTime? cancelledDate)
        {
            return new PartnerPromoCodeLimit()
            {
                CancelDate = cancelledDate,
                Limit = limit
            };
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsNegativeOrZero_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().WithCreatedIsActive(true).Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, CreateBaseSetPartnerPromoCodeLimitRequest(0));

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Fact]
        // Если у партнера есть активный лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes 
        public async void SetPartnerPromoCodeLimit_HasActiveLimit_ClearNumberIssuedPromoCodes()
        {
            // Arrange
            int numberIssuedPromoCodes = 4;
            var partnerId = Guid.NewGuid();
            int testLimit = 5;
            var promoCodeLimit = CreateBasePartnerPromoCodeLimit(testLimit, null);

            var partner = new PartnerBuilder()
                .WithCreatedIsActive(true)
                .WithCreatedNumberIssuePromocodes(numberIssuedPromoCodes)
                .AddLimit(promoCodeLimit)
                .Build();

            var request = CreateBaseSetPartnerPromoCodeLimitRequest(testLimit);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        // Если у партнера нет активного лимита, то не обнуляется количество промокодов, которые партнер выдал NumberIssuedPromoCodes 
        public async void SetPartnerPromoCodeLimit_HasNoActiveLimit_NoClearNumberIssuedPromoCodes()
        {
            // Arrange
            int numberIssuedPromoCodes = 4;
            int testLimit = 5;
            var partnerId = Guid.NewGuid();
            var promoCodeLimit = CreateBasePartnerPromoCodeLimit(testLimit, DateTime.Now);
            var partner = new PartnerBuilder()
                .WithCreatedIsActive(true)
                .WithCreatedNumberIssuePromocodes(numberIssuedPromoCodes)
                .AddLimit(promoCodeLimit)
                .Build();
            var request = CreateBaseSetPartnerPromoCodeLimitRequest(testLimit);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
        }


        [Fact]
        // Если у партнера есть активный промокод, то он д.б. отключен 
        public async void SetPartnerPromoCodeLimit_HasActiveLimit_LimitDisabled()
        {
            // Arrange
            int numberIssuedPromoCodes = 4;
            var partnerId = Guid.NewGuid();
            int testLimit = 5;
            var promoCodeLimit = CreateBasePartnerPromoCodeLimit(testLimit, null);

            var partner = new PartnerBuilder()
                .WithCreatedIsActive(true)
                .WithCreatedNumberIssuePromocodes(numberIssuedPromoCodes)
                .AddLimit(promoCodeLimit)
                .Build();

            var request = CreateBaseSetPartnerPromoCodeLimitRequest(testLimit);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            promoCodeLimit.CancelDate.Should().NotBeNull();
        }
    }
}