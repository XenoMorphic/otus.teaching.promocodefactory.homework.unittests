﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Fixtures
{
    public class InMemoryFixture
    {
        public IServiceProvider ServiceProvider { get; set; }
        public IServiceCollection ServiceCollection { get; set; }
        public InMemoryFixture()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            this.ServiceCollection = Configurator.GetServiceCollection(configuration, null);
            this.ServiceCollection.AddTransient<PartnersController>();
            this.ServiceProvider = GetServiceProvider();
        }
        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();
            SeedData(serviceProvider);
            return serviceProvider;
        }
        private void SeedData(IServiceProvider serviceProvider)
        {
            var initializer = serviceProvider.GetService<IDbInitializer>();
            initializer.InitializeDb();
        }
    }
}
